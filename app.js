const net = require('net')
const spawn = require('child_process').spawn

var client = null

const PORT = 9001
const IP = '10.75.33.101' //192.168.1.50
const yo = parseInt(process.argv[2] || 90)

console.log(`buen día, yo soy ${yo}`)

var imgProcess = null

connectToServer()

function connectToServer(){
  var client = new net.Socket()
  setListeners(client)
}

function setListeners(c){
  c.connect(PORT, IP, () => {
    console.log('conectado al server exitosamente')
    showImg(0)
  })
  c.on('data', (data) => {
    //Parseo msg
    var header = data[0] + data[1]
    var tipo = data[2]
    var n_accion = data[3]
    var val = data[4]
    var footer = data[5] + data[6]
    

    // Me fijo si me compete el mensaje
    if(n_accion === yo){
      console.log('tengo q hacer algo')
      showImg(val)
    }
  })

  c.on('close', () => {
    console.log('cliente cerrado')
    connectToServer()
  })

  c.on('error', (err)=>{
    if(err.code === 'EHOSTUNREACH'){
      console.log('che media pila, el server no está corriendo')
    }
  })
}

function showImg(n){
  console.log(`mostrando imagen ${n}`)
  var kPr = spawn('sudo', ['killall', 'fbi'])
  var iPr = spawn('sudo', ['fbi', '-a', '-T', '1',  '--noverbose', `pic_${n}.png`])
  /*
  kPr.stdout.on('data', (d)=> console.log("kout", d.toString()))
  kPr.stderr.on('data', (d)=> console.log("kerr", d.toString()))
  kPr.on('close', (c)=> console.log("kcode", c))

  iPr.stdout.on('data', (d)=> console.log("iOut", d.toString()))
  iPr.stderr.on('data', (d)=> console.log("iErr", d.toString()))
  iPr.on('close', (c)=> console.log("iCode", c))
  */
}

